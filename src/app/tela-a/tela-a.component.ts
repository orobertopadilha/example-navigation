import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tela-a',
  templateUrl: './tela-a.component.html',
  styleUrls: ['./tela-a.component.css']
})
export class TelaAComponent {

  constructor(private router: Router) { }

    goToB() {
        this.router.navigateByUrl('telaB')
    }

}
