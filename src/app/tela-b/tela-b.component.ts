import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-tela-b',
    templateUrl: './tela-b.component.html',
    styleUrls: ['./tela-b.component.css']
})
export class TelaBComponent {

    constructor(private router: Router) { }

    goToA() {
        this.router.navigateByUrl("telaA")
    }
}
