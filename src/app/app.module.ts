import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TelaAComponent } from './tela-a/tela-a.component';
import { TelaBComponent } from './tela-b/tela-b.component';

@NgModule({
  declarations: [
    AppComponent,
    TelaAComponent,
    TelaBComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
